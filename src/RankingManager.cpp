#include "RankingManager.h"

std::vector<std::string> RankingManager::getRanking () {
// checks if the vector contains something, if not it calls "loadRankings"
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    return _rankings;
}

bool RankingManager::checkRanking (int turns) {
// checks if the score is better than the actual 10th position
    if (turns < std::stoi(_rankings[19])) {
        return true;
    }
    return false;
}

void RankingManager::setRanking (std::string name, int turns) {
// changes the position one by one from the last to the position of the actual score
    int position = 11;
    int i;
    for (i = 10; i > 0; i--) {
        if (turns > stoi(_rankings[i * 2 - 1])) {
            break;
        }
        position--;
    }
    for (i = 10; i > position; i--) {
        _rankings[i * 2 - 1] = _rankings[i * 2 - 3];
        _rankings[i * 2 - 2] = _rankings[i * 2 - 4];
    }
    _rankings[position * 2 - 2] = name;
    _rankings[position * 2 - 1] = std::to_string(turns);
    this->saveRankings ();
}

void RankingManager::saveRankings () {
//TODO: write file with rankings;  
// evens contain the name of the player
// odds contain the number of turns
    std::ofstream file ("ranking.txt");
    if (file.is_open()) {
        for (int i = 0; i<20; i++) {
            file << _rankings[i] << std::endl;
        }
        file.close();
    }
}

void RankingManager::loadRankings () {
//TODO: read file with rankings;
// evens contain the name of the player
// odds contain the number of turns
    std::string line;
    std::ifstream file ("ranking.txt");
    if (file.is_open()) {
        while (getline(file, line)) {
            _rankings.push_back(line);
        }
        file.close();
    }
}

#include "Board.h"
#include "MyFrameListener.h"
#include <iostream>

Board::Board (MyFrameListener* frame_listener, bool human_controlled) {
    _frame_listener = frame_listener;
    _human_controlled = human_controlled;
    _ship_counter = 4;
    _score = 0;
    int i;
    for (i = 0; i < 100; i++) {
        _tiles[i] = new Tile;
    }
}

Board::~Board () {
    int i;
    Ship *ship;
    //delete ships
    for (i = 0; i < 100; i++) {
        if ((ship = _tiles[i]->getShip()) != nullptr) {
            _tiles[i]->setShip(nullptr);
            int j;
            for (j = i + 1; j < 100; j++) {
                if (_tiles[j]->getShip() == ship) {
                    _tiles[j]->setShip(nullptr);
                }
            }
            delete ship;
        }
    }
    for (i = 0; i < 100; i++) {
        delete _tiles[i];
    }
}

bool Board::placeShip (int pos, bool horizontal) {
    int addition = horizontal ? 1 : 10;
    if (!this->isAvailable(pos, _ship_counter + 1, horizontal)) {
        return false;
    }
    Ship* s = new Ship(_ship_counter + 1);
    if (_human_controlled)
        _frame_listener->setShip(pos, horizontal, std::to_string(_ship_counter + 1));
    int i;
    for (i = 0; i < (_ship_counter + 1) * addition; i += addition) {
        _tiles[pos + i]->setShip(s);
    }
    std::cout << "barco colocado en posicion " << pos << " horizontal = " << horizontal << " quedan " << _ship_counter << " barcos" << std::endl;
    _ship_counter--;
    return finished();
}

void Board::hit (int pos) {
    _score++;
    bool drown = _tiles[pos]->hit();
    if (drown) {
        --_ship_counter;
        Ship* s = _tiles[pos]->getShip();
        int i;
        for (i = 0; i<100; i++){
            Ship* aux = _tiles[i]->getShip();
            if (aux == s) {
                _tiles[i]->dead();
                _frame_listener->disableTile(i, DROWN, _human_controlled);
                
            }
        }
    }
    int state = _tiles[pos]->getState();

    _frame_listener->disableTile(pos, state, _human_controlled);
    
    if (this->finished()) {
        _frame_listener->gameOver(_human_controlled);
    }
}

bool Board::finished () {
    if (_ship_counter <= 0) {
        this->_ship_counter = 4;
        return true;
    }
    return false;
}

bool Board::isAvailable(int pos, int size, bool horizontal) {
    int addition = 1;
    if (horizontal) {
        if (((pos % 10) + addition * (size - 1)) >= 10) {
            return false;
        }
    }
    else {
        addition = 10;
        if (pos + (addition * (size - 1)) >= 100) {
            return false;
        }
    }
    int i;
    for (i = 0; i < size * addition; i += addition) {
        if (!_tiles[pos + i]->isEmpty()) {
            return false;
        }
    }
    return true;
}

int Board::getScore(){
	return _score;
}

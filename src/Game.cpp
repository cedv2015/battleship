#include "Game.h"
#include "CPU.h"
#include "Human.h"
#include "MyFrameListener.h"

Game::~Game () {
    if (p1 != nullptr){
        delete p1;
    }
    if (p2 != nullptr){
        delete p2;
    }
}

void Game::createCpuVsCpu () {
    _state = MENU;
    p1 = new CPU(this);
    p2 = new CPU(this);
    setupGame();
}

void Game::createHumanVsCpu() {
    _state = MENU;
    p1 = new Human(this);
    p2 = new CPU(this);
    _frame_listener->setHuman(static_cast<Human*>(p1));
    setupGame();
}

void Game::ready() {
    _ready++;
    if (_ready >= 2) {
        _state = PLAYING;
        _frame_listener->rotateShip();
        srand(time(NULL));
        int starter_player = rand()%2;
        if (starter_player == 1) {
            p1->yourTurn();
        }
        else {
            p2->yourTurn();
        }
    }
}

void Game::setupGame () {
    _ready = 0;
    p1->setOpponent(p2);
    p2->setOpponent(p1);
    p2->placeShips();
    p1->placeShips();
}

void Game::setMyFrameListener(MyFrameListener* frame_listener) {
    _frame_listener = frame_listener;
}

MyFrameListener* Game::getListener() {
    return _frame_listener;
}

void Game::setState(int state){
    _state = state;
}

void Game::gameOver(bool human) {
    _state = GAMEOVER;
}

void Game::restart () {
    delete p1;
    delete p2;
    this->createHumanVsCpu ();
    this->setState(PLACING_SHIPS);
}

Player* Game::getPlayer(){
    return p2;
}

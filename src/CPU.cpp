#include "CPU.h"
#include "Game.h"
#include <random>

CPU::CPU (Game* game) {
    _game = game;
    _my_turn = false;
    _my_board = new Board(_game->getListener(), false);
    int i;
    for (i = 0; i < 100; i++) {
        _tiles_left.push_back(i);
    }
    srand(time(NULL));
}

CPU::~CPU () {
    delete _my_board;
}

// For now it chooses random elements of the board
void CPU::yourTurn () {
    int size = _tiles_left.size();
    if (size > 0){
        int position = rand() % size;
        std::cout << "Posicion " << position << " eliminada con valor " << _tiles_left[position] << ", quedan " << size-1 << " posiciones" << std::endl;
        _opponent->receiveHit(_tiles_left[position]);
        _tiles_left.erase(_tiles_left.begin() + position);
        _opponent->yourTurn();
    }
}

void CPU::placeShips () {
    int position = 0;
    bool horizontal = true;
    do {
        horizontal = rand() % 2 == 1;
        position = rand() % 100;
    } while (!_my_board->placeShip(position, horizontal));
    _game->ready();
}

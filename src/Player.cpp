#include "Player.h"

void Player::setOpponent(Player* p) {
    _opponent = p;
}

void Player::receiveHit (int pos) {
    _my_board->hit(pos);
}

Board* Player::getBoard(){
    return _my_board;
}

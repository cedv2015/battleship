#include "Tile.h"

Tile::Tile () {
    _ship = nullptr;
    _state = INITIAL;
}

Tile::~Tile () {

}

void Tile::setShip (Ship* ship) {
    _ship = ship;
}

bool Tile::isEmpty () {
    if (_ship == nullptr) {
        return true;
    }
    return false;
}

bool Tile::hit () {
    if (_ship == nullptr) {
        _state = WATER;
    }
    else {
        _state = TOUCHED;
        bool dead = _ship->hit();
        if (dead) {
            return true;
        }
    }
    return false;
}

void Tile::dead () {
    _state = DROWN;
}

Ship* Tile::getShip () {
    return _ship;
}

int Tile::getState () {
    return _state;
}

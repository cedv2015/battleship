#include "Human.h"
#include "Game.h"
#include <iostream>

Human::Human (Game* game) {
    _game = game;
    _my_turn = false;
    _my_board = new Board(_game->getListener(), true);
    _horizontal = true;
}

Human::~Human () {
    delete _my_board;
}

void Human::yourTurn () {
    _my_turn = true;
}

void Human::placeShips () {

}

void Human::hit (int pos) {
    int state = _game->getState();
    if (state == PLACING_SHIPS) {
        if (_my_board->placeShip(pos, _horizontal)) {
            _game->ready();
        }
    }
    else if(state == PLAYING) {
        _opponent->receiveHit(pos);
        std::cout << _game->getState() << std::endl;
        if (_game->getState() != GAMEOVER) {
            _opponent->yourTurn();
        }
    }
    _horizontal = true;
}

void Human::changeOrientation () {
    _horizontal = !_horizontal;
}

bool Human::getOrientation () {
    return _horizontal;
}


#include "MyApp.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <vector>

using namespace std;

MyApp::MyApp() {
    _sceneManager = NULL;
    _framelistener = NULL;

    this->initSDL();
}

MyApp::~MyApp() {
    delete _root;
    delete _rankingManager;
}

int MyApp::start() {
    _game = new Game;
    _rankingManager = new RankingManager;
    _rankingManager->getRanking();

    _root = new Ogre::Root();

    if(!_root->restoreConfig()) {
        _root->showConfigDialog();
        _root->saveConfig();
    }

    _trackManager = OGRE_NEW TrackManager;

    Ogre::RenderWindow* window = _root->initialise(true, "Battleship");
    _sceneManager = _root->createSceneManager(Ogre::ST_INTERIOR);
    //_sceneManager->setAmbientLight(Ogre::ColourValue(1,1,1));

    Ogre::Camera* cam = _sceneManager->createCamera("MainCamera");
    cam->setPosition(Ogre::Vector3(0,25,-6));
    cam->lookAt(Ogre::Vector3(0,10,0));
    cam->setNearClipDistance(0.1);
    cam->setFarClipDistance(100);


    Ogre::Viewport* viewport = window->addViewport(cam);
    viewport->setBackgroundColour(Ogre::ColourValue(0.5,0.5,0.5));
    double width = viewport->getActualWidth();
    double height = viewport->getActualHeight();
    cam->setAspectRatio(width / height);

    loadResources();
    createScene();

    _framelistener = new MyFrameListener(window, cam, _sceneManager, _game, _rankingManager);
    _root->addFrameListener(_framelistener);

    createGUI();

    _mainTrack->play();
    _root->startRendering();
    return 0;
}

bool MyApp::initSDL () {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);
 
  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }
 
  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);
 
  return true;    
}

void MyApp::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while(sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void MyApp::createScene() {
    _mainTrack = _trackManager->load("awesomeness.wav");

    _sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);	
    _sceneManager->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneManager->setAmbientLight(Ogre::ColourValue(0.9, 0.9, 0.9));

    _sceneManager->setShadowTextureCount(2);
    _sceneManager->setShadowTextureSize(512);
    
    Ogre::Light* light = _sceneManager->createLight("Light1");
    light->setPosition(0,30,2);
    light->setType(Ogre::Light::LT_SPOTLIGHT);
    light->setDirection(Ogre::Vector3(1,-1,0));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(190.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);

    Ogre::Entity *table_ent = _sceneManager->createEntity("Table", "Table.mesh");
    Ogre::SceneNode *table_node = _sceneManager->createSceneNode("Table_Node");
    _sceneManager->getRootSceneNode()->addChild(table_node);
    table_node->attachObject(table_ent);
    table_node->setVisible(true);
    table_node->scale(Ogre::Vector3(2,1,2));
    table_node->translate(Ogre::Vector3(0,-10,0));
    table_ent->setQueryFlags(BOARD);

    Ogre::Entity *selector = _sceneManager->createEntity("Selector", "Selector.mesh");
    Ogre::SceneNode *node_selector = _sceneManager->createSceneNode("Selector_Node");
    _sceneManager->getRootSceneNode()->addChild(node_selector);
    node_selector->attachObject(selector);
    node_selector->setVisible(false);
    selector->setQueryFlags(BOARD);

    Ogre::Entity *banner = _sceneManager->createEntity("BATTLESHIP", "BATTLESHIP.mesh");
    Ogre::SceneNode *node_banner = _sceneManager->createSceneNode("BATTLESHIP_Node");
    _sceneManager->getRootSceneNode()->addChild(node_banner);
    node_banner->attachObject(banner);
    node_banner->yaw(Ogre::Degree(180));
    node_banner->pitch(Ogre::Degree(30));
    node_banner->translate(Ogre::Vector3(-0.5,15,0));
    node_banner->scale(Ogre::Vector3(2,2,2));

    Ogre::SceneNode *node_rotate = _sceneManager->createSceneNode("Rotate_Node");
    _sceneManager->getRootSceneNode()->addChild(node_rotate);
    Ogre::Entity *ent = _sceneManager->createEntity("Board", "Board.mesh");
    Ogre::SceneNode *node1 = _sceneManager->createSceneNode("Board_Node");
    ent->setQueryFlags(BOARD);
    _sceneManager->getSceneNode("Rotate_Node")->addChild(node1);
    node1->attachObject(ent);
    vector<Ogre::Entity*> colisions;
    std::stringstream sauxnode;
    string s = "Box_Col";
    string e = "Enemy_Box_Col";
    for (int i=0; i<100; i++) {
        sauxnode << s << i;
        Ogre::SceneNode *nodebox = _sceneManager->createSceneNode(sauxnode.str());
        Ogre::Entity *entboxcol = _sceneManager->createEntity(sauxnode.str(), "Box_Collision.mesh");
        _sceneManager->getRootSceneNode()->addChild(nodebox);
        colisions.push_back(entboxcol);
        entboxcol->setQueryFlags(TILE);
        nodebox->attachObject(entboxcol);
        nodebox->translate(Ogre::Vector3(-(-4.5+(i%10)),0.19801,4.5-(i/10)));  //-X,Z,Y
        nodebox->setVisible(false);
        sauxnode.str("");
    }    

    node1->translate(Ogre::Vector3(0,0,0));

    Ogre::Entity *ent_enemy = _sceneManager->createEntity("Board_Enemy", "Board.mesh");
    Ogre::SceneNode *node2 = _sceneManager->createSceneNode("Board_Enemy_Node");
    ent_enemy->setQueryFlags(BOARD);
    _sceneManager->getSceneNode("Rotate_Node")->addChild(node2);
    node2->attachObject(ent_enemy);
    node2->pitch(Ogre::Degree(-90));
    node2->translate(Ogre::Vector3(0,5,6));


    s = "Enemy_Box_Col";
    for (int i=0; i<100; i++) {
        sauxnode << s << i;
        Ogre::SceneNode *nodebox = _sceneManager->createSceneNode(sauxnode.str());
        Ogre::Entity *entboxcol = _sceneManager->createEntity(sauxnode.str(), "Box_Collision.mesh");
        _sceneManager->getRootSceneNode()->addChild(nodebox);
        colisions.push_back(entboxcol);
        entboxcol->setQueryFlags(TILE);
        nodebox->attachObject(entboxcol);
        nodebox->translate(Ogre::Vector3(0,5,6)); //Transformacion que se aplico al tablero enemigo
        nodebox->pitch(Ogre::Degree(-90), Ogre::Node::TransformSpace::TS_WORLD);
        nodebox->translate(Ogre::Vector3(-(-4.5+(i%10)),4.5-(i/10),-0.19801));  //-X,Z,Y
        nodebox->setVisible(false);
        sauxnode.str("");
    
        
  } 

    Ogre::Entity *ent_ship2 = _sceneManager->createEntity("Ship2", "Ship2.mesh");
    ent_ship2->setQueryFlags(BOARD);
    Ogre::SceneNode *node_ship2 = _sceneManager->createSceneNode("Ship2_Node");
    _sceneManager->getRootSceneNode()->addChild(node_ship2);
    node_ship2->attachObject(ent_ship2);
    node_ship2->yaw(Ogre::Degree(-90));
    node_ship2->translate(Ogre::Vector3(0,100,0)); 
    node_ship2->setVisible(false);

    Ogre::Entity *ent_ship3 = _sceneManager->createEntity("Ship3", "Ship3.mesh");
    ent_ship3->setQueryFlags(BOARD);
    Ogre::SceneNode *node_ship3 = _sceneManager->createSceneNode("Ship3_Node");
    _sceneManager->getRootSceneNode()->addChild(node_ship3);
    node_ship3->attachObject(ent_ship3);
    node_ship3->yaw(Ogre::Degree(-90));
    node_ship3->translate(Ogre::Vector3(0,100,0)); 
    node_ship3->setVisible(false);

    Ogre::Entity *ent_ship4 = _sceneManager->createEntity("Ship4", "Ship4.mesh");
    ent_ship4->setQueryFlags(BOARD);
    Ogre::SceneNode *node_ship4 = _sceneManager->createSceneNode("Ship4_Node");
    _sceneManager->getRootSceneNode()->addChild(node_ship4);
    node_ship4->attachObject(ent_ship4);
    node_ship4->yaw(Ogre::Degree(-90));
    node_ship4->translate(Ogre::Vector3(0,100,0)); 
    node_ship4->setVisible(false);

    Ogre::Entity *ent_ship5 = _sceneManager->createEntity("Ship5", "Ship5.mesh");
    ent_ship5->setQueryFlags(BOARD);
    Ogre::SceneNode *node_ship5 = _sceneManager->createSceneNode("Ship5_Node");
    _sceneManager->getRootSceneNode()->addChild(node_ship5);
    node_ship5->attachObject(ent_ship5);
    node_ship5->yaw(Ogre::Degree(-90));
    node_ship5->translate(Ogre::Vector3(0,100,0)); 
    node_ship5->setVisible(false);

    _sceneManager->getSceneNode("Rotate_Node")->setInitialState();
    
}


void MyApp::createGUI()
{
  //CEGUI
  renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("Adore64-12");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

  //Sheet
  CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/Sheet");
  _framelistener->setSheet(sheet);

  //Start button
  CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/PlayButton");
  playButton->setText("Play");
  playButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.5,0)));
  playButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MyFrameListener::play, 
						      _framelistener));

  //Records button
  CEGUI::Window* recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/RecordsButton");
  recordsButton->setText("Records");
  recordsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.55,0)));
  recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MyFrameListener::records, 
						      _framelistener));

  //Quit button
  CEGUI::Window* creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/CreditsButton");
  creditsButton->setText("Credits");
  creditsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.6,0)));
  creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MyFrameListener::credits, 
						      _framelistener));

  //Quit button
  CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/QuitButton");
  quitButton->setText("Exit");
  quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.9-0.15/2,0),CEGUI::UDim(0.65,0)));
  quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MyFrameListener::quit, 
						      _framelistener));
  //Attaching buttons
  sheet->addChild(playButton);
  sheet->addChild(recordsButton);
  sheet->addChild(creditsButton);
  sheet->addChild(quitButton);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}


#include "MyFrameListener.h"
#include "Game.h"
#include "Human.h"
#include <sstream>

MyFrameListener::MyFrameListener(Ogre::RenderWindow* win, Ogre::Camera* cam, Ogre::SceneManager* sm, Game* game, RankingManager* rankingManager) {
    OIS::ParamList param;
    std::size_t windowHandle;
    std::ostringstream wHandleStr;

    _human = 0;
    _camera = cam;
    _sceneManager = sm;
    _win = win;
    _game = game;
    _game->setMyFrameListener(this);
    _game->createHumanVsCpu();
    _quit = false;
    _next_ship = 5;
    _hide = false;
    _sheet = NULL, _records = NULL, _credits = NULL;
    _start = true;

    _win->getCustomAttribute("WINDOW", &windowHandle);
    wHandleStr << windowHandle;
    param.insert(make_pair("WINDOW", wHandleStr.str()));

    _inputManager = OIS::InputManager::createInputSystem(param);
    _keyboard = static_cast<OIS::Keyboard*>(_inputManager->createInputObject(OIS::OISKeyboard, false));
    _mouse = static_cast<OIS::Mouse*>(_inputManager->createInputObject(OIS::OISMouse, true));
    _mouse->getMouseState().width = _win->getWidth();
    _mouse->getMouseState().height = _win->getHeight();

    _keyboard->setEventCallback(this);
    _mouse->setEventCallback(this);

    _raySceneQuery = _sceneManager->createRayQuery(Ogre::Ray());
    _rankingManager = rankingManager;
    _letters[1] = 1;
    _letters[2] = 1;
    _letters[3] = 1;
}

MyFrameListener::~MyFrameListener() {
    _inputManager->destroyInputObject(_keyboard);
    _inputManager->destroyInputObject(_mouse);
    _sceneManager->destroyQuery(_raySceneQuery);
    OIS::InputManager::destroyInputSystem(_inputManager);
    delete _game;
}

Ogre::Ray MyFrameListener::setRayQuery(int posx, int posy, Ogre::uint32 mask) {
    Ogre::Ray rayMouse = _camera->getCameraToViewportRay(posx/float(_win->getWidth()), posy/float(_win->getHeight()));
    _raySceneQuery->setRay(rayMouse);
    _raySceneQuery->setSortByDistance(true);
    _raySceneQuery->setQueryMask(mask);
    return (rayMouse);
}

void MyFrameListener::setHuman (Human* human) {
    _human = human;
}

bool MyFrameListener::frameStarted(const Ogre::FrameEvent& evt) {
    Ogre::Vector3 vt(0,0,0);
    Ogre::Real tSpeed = 10.0;
    Ogre::Real deltaT = evt.timeSinceLastFrame;
    int fps = 1.0 / deltaT;
    bool mbleft, mbright;

    if((_game->getState()) == MENU){
        _sceneManager->getSceneNode("Rotate_Node")->roll(Ogre::Degree(0.1));
        _sceneManager->getSceneNode("Rotate_Node")->yaw(Ogre::Degree(0.1));
        _sceneManager->getSceneNode("Rotate_Node")->pitch(Ogre::Degree(0.1));
    }

    _keyboard->capture();
    _mouse->capture();

    int posx = _mouse->getMouseState().X.abs;
    int posy = _mouse->getMouseState().Y.abs;

    if(_keyboard->isKeyDown(OIS::KC_ESCAPE)) return false; // Exit
    if(_game->getState() == PLACING_SHIPS) {
            if (_human != nullptr) {
                std::string name = std::string("Ship") + std::to_string(_human->getNextShip()) + std::string("_Node");
                Ogre::SceneNode* node = _sceneManager->getSceneNode(name);
                node->setPosition(Ogre::Vector3(100, 0, 0));
                node->setVisible(true);
            }
    }
    if((_game->getState() == PLACING_SHIPS) || (_game->getState() == PLAYING)){
        _sheet->disable();
        _sheet->setVisible(false);
    }else{
        _sheet->enable();
        _sheet->setVisible(true);
    }
    // capture left click
    mbleft = _mouse->getMouseState().buttonDown(OIS::MB_Left);
    mbright = _mouse->getMouseState().buttonDown(OIS::MB_Right);

    if (mbleft) {
        if (!_lastclicked) {
            _lastclicked = true;
            std::cout << "Left click..." << std::endl;
            Ogre::uint32 mask = TILE;
            Ogre::Ray r = setRayQuery(posx, posy, mask);
            Ogre::RaySceneQueryResult& result = _raySceneQuery->execute();
            Ogre::RaySceneQueryResult::iterator it;
            it = result.begin();
            if(it != result.end()) {
                std::string name_box, string_num;
                name_box = it->movable->getName();
                std::cout << "MovableObject: " << name_box << std::endl;
                if(it->movable->getName().substr(0, 7) == "Box_Col"){
                    std::cout << "Tile pressed" << std::endl;
                    string_num = name_box.substr(7,9);
                    int position = stoi(string_num);
                    if(_human != nullptr) {
                        _human->hit(position);
                    }
                }
                if(it->movable->getName().substr(0, 5) == "Arrow"){
                    std::string arrow = it->movable->getName().substr(5);
                    std::cout << "Arrow pressed" << arrow << std::endl;
                    changeLetter(arrow);
                    
                }
                
            }
        }
    }
    else if (mbright) {
        if (!_lastclicked) {
            _lastclicked = true;
            std::cout << "Right click..." << std::endl;
            if (_human != nullptr) {
                if (_game->getState() == PLACING_SHIPS) {
                    std::string name = std::string("Ship") + std::to_string(_human->getNextShip()) + std::string("_Node");
                    Ogre::SceneNode* node = _sceneManager->getSceneNode(name);
                    if (_human->getOrientation()) {
                        node->yaw(Ogre::Degree(-90));
                    }
                    else {
                        node->yaw(Ogre::Degree(90));
                    }
                    _human->changeOrientation();
                }
            }
        }
    }
    else {
        _lastclicked = false;
        if (_game->getState() == PLACING_SHIPS) {
            Ogre::uint32 mask = TILE;
            Ogre::Ray r = setRayQuery(posx, posy, mask);
            Ogre::RaySceneQueryResult& result = _raySceneQuery->execute();
            Ogre::RaySceneQueryResult::iterator it;
            it = result.begin();
            if(it != result.end()) {
                std::string name_box, string_num;
                name_box = it->movable->getName();
                if(it->movable->getName().substr(0, 7) == "Box_Col"){
                    string_num = name_box.substr(7,9);
                    int position = stoi(string_num);
                    if(_human != nullptr) {
                        int size = _human->getNextShip();
                        if(_human->isAvailable(position, size, _human->getOrientation())) {
                            //draw the ship with 50% alpha
                            std::string auxname = std::string("Ship") + std::to_string(size) + std::string("_Node");
                            Ogre::SceneNode *node = _sceneManager->getSceneNode(auxname);
                            int separation = 1;
                            int posX = (position % 10) * separation;
                            int posY = (position / 10) * separation;
                            node->setPosition (Ogre::Vector3(4.5 - posX * separation, 0, 4.5 - posY * separation));
                        }
                        else {
                            // place the ships away from the camera again
                            std::string name = std::string("Ship") + std::to_string(_human->getNextShip()) + std::string("_Node");
                            this->placeShipAway(name);
                        }
                    }
                }
                else {
                    std::string name = std::string("Ship") + std::to_string(_human->getNextShip());
                    if (it->movable->getName() != name) {
                        // place the ships away from the camera again
                        name += std::string("_Node");
                        this->placeShipAway(name);
                    }
                }
            }
        }
        else if (_game->getState() == PLAYING) {
            Ogre::uint32 mask = TILE;
            Ogre::Ray r = setRayQuery(posx, posy, mask);
            Ogre::RaySceneQueryResult& result = _raySceneQuery->execute();
            Ogre::RaySceneQueryResult::iterator it;
            it = result.begin();
            if(it != result.end()) {
                std::string name_box = it->movable->getName();
                if(it->movable->getName().substr(0, 7) == "Box_Col"){
                    Ogre::SceneNode *node = _sceneManager->getSceneNode(name_box);
                    Ogre::SceneNode *selector = _sceneManager->getSceneNode("Selector_Node");
                    selector->setVisible(true);
                    selector->setPosition(node->getPosition());
                }
            }
            else{
                this->restartSelector();
            }
        }
    }

    if(_quit) return false;

    return true;
}


void MyFrameListener::placeShipAway(std::string ship) {
    Ogre::SceneNode *node = _sceneManager->getSceneNode(ship);
    node->setPosition(Ogre::Vector3(-100, 0, 0));
}

void MyFrameListener::setShip(int position, bool horizontal, std::string ship) {
    int separation = 1;
    std::string name = std::string("Ship") + ship;
    Ogre::Entity* node = _sceneManager->getEntity(name);
    name +=  std::string("_Node");
    Ogre::SceneNode* scene_node = _sceneManager->getSceneNode(name);
    node->setMaterialName("Material_ship.solid");
    int posX = (position % 10) * separation;
    int posY = (position / 10) * separation;
    scene_node->setPosition (Ogre::Vector3(4.5 - posX * separation, 0, 4.5 - posY * separation));
    scene_node->setVisible(true);
}

bool MyFrameListener::keyPressed(const OIS::KeyEvent& evt)
{
  if(evt.key==OIS::KC_ESCAPE) return _quit=true;

  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

  return true;
}

bool MyFrameListener::keyReleased(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));
  return true;
}

bool MyFrameListener::mouseMoved(const OIS::MouseEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(evt.state.X.abs, evt.state.Y.abs);
  return true;
}

bool MyFrameListener::mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
  return true;
}

bool MyFrameListener::mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
  return true;
}

CEGUI::MouseButton MyFrameListener::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}


bool MyFrameListener::quit(const CEGUI::EventArgs &e)
{
  _quit = true;
  return true;
}

bool MyFrameListener::credits(const CEGUI::EventArgs &e)
{
  CEGUI::Window* creditsWin;
  if(_credits == NULL){
    creditsWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("credits.layout");
    _credits = creditsWin;
    _sheet->addChild(creditsWin);
    CEGUI::Window* exitButton = creditsWin->getChild("ExitB");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::hideC, this));

  }else{
    _credits->show();
  }
  return true;
}

bool MyFrameListener::play(const CEGUI::EventArgs &e)
{
  _game->setState(PLACING_SHIPS);
  try{
      _sceneManager->destroySceneNode("BATTLESHIP_Node");
      _sceneManager->destroyEntity("BATTLESHIP");
  }catch(Ogre::ItemIdentityException e){}
   _sceneManager->getSceneNode("Rotate_Node")->resetToInitialState();
  
  if(!_start)
      MyFrameListener::restartScene();
  _start = false;
  
  return true;
}

bool MyFrameListener::records(const CEGUI::EventArgs &e)
{
    CEGUI::Window* recordsWin;
    if(_records == NULL){
        recordsWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("configWin.layout");
        _records = recordsWin;
        _sheet->addChild(recordsWin);
        CEGUI::Window* exitButton = recordsWin->getChild("ExitButton");
        exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyFrameListener::hide, this));

        std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i=0; i<ranking.size(); i+=2){
            name = std::string("Name") + std::to_string(i/2+1) + std::string("Text");
            score = std::string("Score") + std::to_string(i/2+1) + std::string("Text");
            CEGUI::Window* name_record = recordsWin->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = recordsWin->getChild(score);
            score_record ->setText(ranking[i+1]);
    }

    }else{
        std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i = 0; i < ranking.size(); i += 2) {
            name = std::string("Name") + std::to_string(i / 2 + 1) + std::string("Text");
            score = std::string("Score") + std::to_string(i / 2 + 1) + std::string("Text");
            CEGUI::Window* name_record = _records->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = _records->getChild(score);
            score_record ->setText(ranking[i+1]);
        }
        _records->show();
    }

    
  return true;
}

void MyFrameListener::setSheet(CEGUI::Window* sheet)
{
  _sheet = sheet;

}

bool MyFrameListener::hide(const CEGUI::EventArgs &e)
{
  _records->hide();
  return true;

}

bool MyFrameListener::hideC(const CEGUI::EventArgs &e)
{
  _credits->hide();
  return true;

}

bool MyFrameListener::save(const CEGUI::EventArgs &e)
{
        
    char * iniciales;
    for (int i=1; i <= 3; i++){
        iniciales[i-1] =  (char) _letters[i]+64;
    }

    std::string name(iniciales, 3);
    int score = _game->getPlayer()->getBoard()->getScore();
    if (_rankingManager->checkRanking(score)){
        _rankingManager->setRanking(name,score);
    }
    deleteAddRecord();
    _save->disable();
    _save->setVisible(false);
    
    return true;

}

void MyFrameListener::rotateShip(){

  for(int i=2; i<=5; i++){
    std::string ship = std::to_string(i);
    std::string name = std::string("Ship") + ship + std::string("_Node");
    Ogre::SceneNode *node = _sceneManager->getSceneNode(name);
    Ogre::Vector3 position = node->getPosition();
    Ogre::Real x = position.x;
    Ogre::Real y = position.y;
    Ogre::Real z = position.z;
    
    node->translate(Ogre::Vector3(x*(-1),y*(-1),z*(-1)));
    node->translate(Ogre::Vector3(0,5,6)); //Transformacion que se aplico al tablero enemigo
    node->pitch(Ogre::Degree(-90), Ogre::Node::TransformSpace::TS_WORLD);
    node->translate(Ogre::Vector3(x,z,y*(-1)));
    
  }
  
}


void MyFrameListener::disableTile (int pos, int state, bool human) {
    //la variable human no sería funcional cuando el juego sea un humano contra humano, debe ser cuando sea el propio tablero o el enemigo
    std::string name;
    if(human){
        name = std::string("Enemy_Box_Col") + std::to_string(pos);
    }else{
        name = std::string("Box_Col") + std::to_string(pos);
    }
    Ogre::Entity* node = _sceneManager->getEntity(name);
    node->setQueryFlags(TILE_DISABLED);
    Ogre::SceneNode* scene_node = _sceneManager->getSceneNode(name);
    scene_node->setVisible(true);
    if(state==1)
        node->setMaterialName("Material.agua");
    else if(state==2)
        node->setMaterialName("Material.tocado");
    else if(state==3)
        node->setMaterialName("Material.hundido");
}

void MyFrameListener::gameOver(bool human) {
    _game->gameOver(human);
    std::string name;
    if (!human)
        name = std::string("WIN");
    else
        name = std::string("LOSE");
    std::string name_mesh = name + std::string(".mesh");
    std::string name_node = name + std::string("_Node");
    Ogre::Entity *banner = _sceneManager->createEntity(name, name_mesh);
    Ogre::SceneNode *node_banner = _sceneManager->createSceneNode(name_node);
    _sceneManager->getRootSceneNode()->addChild(node_banner);
    node_banner->attachObject(banner);
    node_banner->yaw(Ogre::Degree(180));
    node_banner->pitch(Ogre::Degree(30));
    node_banner->translate(Ogre::Vector3(0,12,2));
    node_banner->scale(Ogre::Vector3(2,2,2));


    if(_rankingManager->checkRanking(_game->getPlayer()->getBoard()->getScore()))
        addRecord();
}

void MyFrameListener::restartScene () {
    
    try{
        _sceneManager->destroySceneNode("LOSE_Node");
        _sceneManager->destroyEntity("LOSE");

    }catch(Ogre::ItemIdentityException e){

        try{
            _sceneManager->destroySceneNode("WIN_Node");
            _sceneManager->destroyEntity("WIN");
            try{
                deleteAddRecord();
            }catch(Ogre::ItemIdentityException e){}
        }catch(Ogre::ItemIdentityException e){}
    }

    int i;
    std::string name;
    // change the tiles to their original state
    for (i = 0; i < 100; i++) {
        name = std::string("Box_Col") + std::to_string(i);
        _sceneManager->getSceneNode(name)->setVisible(false);
        _sceneManager->getEntity(name)->setQueryFlags(TILE);
        name = std::string("Enemy_Box_Col") + std::to_string(i);
        _sceneManager->getSceneNode(name)->setVisible(false);
    }
    // change the ships to invisible and to their original position
    for (i = 2; i < 6; i++) {
        name = std::string("Ship") + std::to_string(i);
        Ogre::Entity* node = _sceneManager->getEntity(name);
        name += std::string("_Node");
        _sceneManager->getSceneNode(name)->setVisible(false);
        Ogre::Vector3 position = _sceneManager->getSceneNode(name)->getPosition();
        Ogre::Real x = position.x;
        Ogre::Real y = position.y;
        Ogre::Real z = position.z;
        
        _sceneManager->getSceneNode(name)->translate(Ogre::Vector3(x*(-1),y*(-1),z*(-1)));
        _sceneManager->getSceneNode(name)->translate(Ogre::Vector3(0,-5,-6)); //Transformacion que se aplico al tablero enemigo
        _sceneManager->getSceneNode(name)->resetOrientation();
        _sceneManager->getSceneNode(name)->yaw(Ogre::Degree(-90));
        node->setMaterialName("Material_ship");
    }
    this->restartSelector();
    _game->restart();
}

void MyFrameListener::restartSelector() {
    Ogre::SceneNode *node = _sceneManager->getSceneNode("Selector_Node");
    node->setVisible(false);
}

void MyFrameListener::addRecord() {


    _letters[1] = 1;
    _letters[2] = 1;
    _letters[3] = 1;

//Primera letra
    Ogre::Entity *firstLetterEntity = _sceneManager->createEntity("1Letter", "A.mesh");
    Ogre::SceneNode *firstLetterNode = _sceneManager->createSceneNode("1Letter_Node");
    _sceneManager->getRootSceneNode()->addChild(firstLetterNode);
    firstLetterNode->attachObject(firstLetterEntity);
    firstLetterNode->yaw(Ogre::Degree(180));
    firstLetterNode->pitch(Ogre::Degree(30));
    firstLetterNode->translate(Ogre::Vector3(3,9,-2));
    firstLetterNode->scale(Ogre::Vector3(2,2,2));

    //Flecha superior primera letra
    Ogre::Entity *firstLetterArrowUpEntity = _sceneManager->createEntity("Arrow1U", "arrow.mesh");
    Ogre::SceneNode *firstLetterArrowUpNode = _sceneManager->createSceneNode("Arrow1U_Node");
    _sceneManager->getRootSceneNode()->addChild(firstLetterArrowUpNode);
    firstLetterArrowUpNode->attachObject(firstLetterArrowUpEntity);
    firstLetterArrowUpNode->yaw(Ogre::Degree(180));
    firstLetterArrowUpNode->translate(Ogre::Vector3(3,9,-0.5));

    //Flecha inferior primera letra
    Ogre::Entity *firstLetterArrowDownEntity = _sceneManager->createEntity("Arrow1D", "arrow.mesh");
    Ogre::SceneNode *firstLetterArrowDownNode = _sceneManager->createSceneNode("Arrow1D_Node");
    _sceneManager->getRootSceneNode()->addChild(firstLetterArrowDownNode);
    firstLetterArrowDownNode->attachObject(firstLetterArrowDownEntity);
    firstLetterArrowDownNode->translate(Ogre::Vector3(3,9,-3.5));

    //Segunda letra
    Ogre::Entity *secondLetterEntity = _sceneManager->createEntity("2Letter", "A.mesh");
    Ogre::SceneNode *secondLetterNode = _sceneManager->createSceneNode("2Letter_Node");
    _sceneManager->getRootSceneNode()->addChild(secondLetterNode);
    secondLetterNode->attachObject(secondLetterEntity);
    secondLetterNode->yaw(Ogre::Degree(180));
    secondLetterNode->pitch(Ogre::Degree(30));
    secondLetterNode->translate(Ogre::Vector3(0,9,-2));
    secondLetterNode->scale(Ogre::Vector3(2,2,2));

    //Flecha superior segunda letra
    Ogre::Entity *secondLetterArrowUpEntity = _sceneManager->createEntity("Arrow2U", "arrow.mesh");
    Ogre::SceneNode *secondLetterArrowUpNode = _sceneManager->createSceneNode("Arrow2U_Node");
    _sceneManager->getRootSceneNode()->addChild(secondLetterArrowUpNode);
    secondLetterArrowUpNode->attachObject(secondLetterArrowUpEntity);
    secondLetterArrowUpNode->yaw(Ogre::Degree(180));
    secondLetterArrowUpNode->translate(Ogre::Vector3(0,9,-0.5));

    //Flecha inferior segunda letra
    Ogre::Entity *secondLetterArrowDownEntity = _sceneManager->createEntity("Arrow2D", "arrow.mesh");
    Ogre::SceneNode *secondLetterArrowDownNode = _sceneManager->createSceneNode("Arrow2D_Node");
    _sceneManager->getRootSceneNode()->addChild(secondLetterArrowDownNode);
    secondLetterArrowDownNode->attachObject(secondLetterArrowDownEntity);
    secondLetterArrowDownNode->translate(Ogre::Vector3(0,9,-3.5));

    //Tercera letra
    Ogre::Entity *thirdLetterEntity = _sceneManager->createEntity("3Letter", "A.mesh");
    Ogre::SceneNode *thirdLetterNode = _sceneManager->createSceneNode("3Letter_Node");
    _sceneManager->getRootSceneNode()->addChild(thirdLetterNode);
    thirdLetterNode->attachObject(thirdLetterEntity);
    thirdLetterNode->yaw(Ogre::Degree(180));
    thirdLetterNode->pitch(Ogre::Degree(30));
    thirdLetterNode->translate(Ogre::Vector3(-3,9,-2));
    thirdLetterNode->scale(Ogre::Vector3(2,2,2));

    //Flecha superior tercera letra
    Ogre::Entity *thirdLetterArrowUpEntity = _sceneManager->createEntity("Arrow3U", "arrow.mesh");
    Ogre::SceneNode *thirdLetterArrowUpNode = _sceneManager->createSceneNode("Arrow3U_Node");
    _sceneManager->getRootSceneNode()->addChild(thirdLetterArrowUpNode);
    thirdLetterArrowUpNode->attachObject(thirdLetterArrowUpEntity);
    thirdLetterArrowUpNode->yaw(Ogre::Degree(180));
    thirdLetterArrowUpNode->translate(Ogre::Vector3(-3,9,-0.5));

    //Flecha inferior tercera letra
    Ogre::Entity *thirdLetterArrowDownEntity = _sceneManager->createEntity("Arrow3D", "arrow.mesh");
    Ogre::SceneNode *thirdLetterArrowDownNode = _sceneManager->createSceneNode("Arrow3D_Node");
    _sceneManager->getRootSceneNode()->addChild(thirdLetterArrowDownNode);
    thirdLetterArrowDownNode->attachObject(thirdLetterArrowDownEntity);
    thirdLetterArrowDownNode->translate(Ogre::Vector3(-3,9,-3.5));

    _save = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/SaveButton");
    _save->setText("Save");
    _save->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    _save->setPosition(CEGUI::UVector2(CEGUI::UDim(0.425,0),CEGUI::UDim(0.9,0)));
    _save->subscribeEvent(CEGUI::PushButton::EventClicked,
		         CEGUI::Event::Subscriber(&MyFrameListener::save, 
					          this));
    _save->subscribeEvent(CEGUI::PushButton::EventClicked,
		         CEGUI::Event::Subscriber(&MyFrameListener::records, 
					          this));
    //Attaching buttons
    _sheet->addChild(_save);
}

void MyFrameListener::changeLetter(std::string arrow){


    int order = (int)arrow[0] -48;
    char direction = arrow[1];
    std::string name = std::to_string(order) + std::string("Letter");
    std::string node = name + std::string("_Node");
    _sceneManager->destroyEntity(name);

    if(direction == 'U'){
        _letters[order]++;
        if(_letters[order]>26) _letters[order] = 1;
    }else{
        _letters[order]--;
        if(_letters[order]<1) _letters[order] = 26;
    }

    char mesh = (char) _letters[order]+64;std::stringstream ss;
    std::string mesh_name;
    ss << mesh;
    ss >> mesh_name;

    Ogre::Entity *letterEntity = _sceneManager->createEntity(name, mesh_name+std::string(".mesh"));
    _sceneManager->getSceneNode(node)->attachObject(letterEntity);

}

void MyFrameListener::deleteAddRecord(){
    _sceneManager->destroySceneNode("1Letter_Node");
    _sceneManager->destroyEntity("1Letter");
    _sceneManager->destroySceneNode("2Letter_Node");
    _sceneManager->destroyEntity("2Letter");
    _sceneManager->destroySceneNode("3Letter_Node");
    _sceneManager->destroyEntity("3Letter");
    _sceneManager->destroySceneNode("Arrow1U_Node");
    _sceneManager->destroyEntity("Arrow1U");
    _sceneManager->destroySceneNode("Arrow1D_Node");
    _sceneManager->destroyEntity("Arrow1D");
    _sceneManager->destroySceneNode("Arrow2U_Node");
    _sceneManager->destroyEntity("Arrow2U");
    _sceneManager->destroySceneNode("Arrow2D_Node");
    _sceneManager->destroyEntity("Arrow2D");
    _sceneManager->destroySceneNode("Arrow3U_Node");
    _sceneManager->destroyEntity("Arrow3U");
    _sceneManager->destroySceneNode("Arrow3D_Node");
    _sceneManager->destroyEntity("Arrow3D");

    
}

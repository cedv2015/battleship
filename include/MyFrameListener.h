#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "RankingManager.h"

#define TILE 1 << 0
#define TILE_DISABLED 1 << 1
#define BOARD 1 << 2

class Game;
class Human;
class MyFrameListener : public Ogre::FrameListener, OIS::KeyListener, OIS::MouseListener {
    private:
      OIS::InputManager* _inputManager;
      OIS::Keyboard* _keyboard;
      OIS::Mouse* _mouse;
      Ogre::Camera* _camera;
      Ogre::SceneNode *_node; //
      CEGUI::OgreRenderer* renderer; 
      int _next_ship;
      CEGUI::Window* _sheet, *_records, *_save, *_credits;

      Ogre::RenderWindow* _win;
      Ogre::SceneManager* _sceneManager;
      RankingManager* _rankingManager;
      Ogre::RaySceneQuery* _raySceneQuery;
      Ogre::Ray setRayQuery(int posx, int posy, Ogre::uint32 mask);
      Game* _game;
      Human* _human;

      int _letters[];


      bool _lastclicked = false, _start;

      bool keyPressed(const OIS::KeyEvent& evt);
      bool keyReleased(const OIS::KeyEvent& evt);
      bool mouseMoved(const OIS::MouseEvent& evt);
      bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
      bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

      bool _quit;
      bool _hide;
      float _timeSinceLastFrame;

      CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
      void placeShipAway(std::string ship);

public:
      MyFrameListener(Ogre::RenderWindow* win, Ogre::Camera* cam, Ogre::SceneManager* sm, Game* game, RankingManager *rankingManager);
      ~MyFrameListener();
      bool frameStarted(const Ogre::FrameEvent& evt);
      //bool frameEnded(const Ogre::FrameEvent& evt);

      bool play(const CEGUI::EventArgs &e);  
      bool records(const CEGUI::EventArgs &e); 
      bool quit(const CEGUI::EventArgs &e);   
      bool hide(const CEGUI::EventArgs &e);    
      bool hideC(const CEGUI::EventArgs &e);
      bool save(const CEGUI::EventArgs &e);
      bool credits(const CEGUI::EventArgs &e);
      void setHuman (Human* human);
      void setSheet(CEGUI::Window* sheet);
      void setShip(int position, bool horizontal, std::string ship);
      void rotateShip();
      void disableTile(int pos, int state, bool human);
      void gameOver(bool human);
      void restartScene ();
      void restartSelector ();
      void addRecord();
      void changeLetter(std::string arrow);
      void deleteAddRecord();
};

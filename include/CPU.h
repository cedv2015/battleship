#include "Player.h"
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <iostream>

class CPU : public Player {
    private:
        std::vector<int> _tiles_left;
    public:
        CPU (Game* game);
        ~CPU ();
        virtual void yourTurn ();
        virtual void placeShips ();
};

#include "Player.h"

#define MENU 0
#define PLACING_SHIPS 1
#define PLAYING 2
#define GAMEOVER 3

class Human;
class CPU;
class MyFrameListener;
class Game {
    private:
        Player* p1;
        Player* p2;
        MyFrameListener* _frame_listener;
        int _state;
        int _ready;
        void setupGame ();
    public:
        Game() : _state(MENU) {};
        ~Game ();
        void createCpuVsCpu ();
        void createHumanVsCpu ();
        void ready ();
        void setMyFrameListener (MyFrameListener* frame_listener);
        int getState () { return _state; };
        MyFrameListener* getListener ();
        void setState (int state);
        void gameOver (bool human);
        void restart ();
        Player* getPlayer();
};

#ifndef PLAYER_H
#define PLAYER_H
#include "Board.h"
class Game;
class Player {
    protected:
        Player* _opponent;
        bool _my_turn;
        Board* _my_board;
        Game* _game;
    public:
        void setOpponent(Player* p);
        virtual void yourTurn () = 0;
        virtual void placeShips () = 0;
        void receiveHit (int pos);
        virtual ~Player () {};
        Board* getBoard();
};
#endif

#include "Tile.h"

class MyFrameListener;
class Board {
    private:
        Tile* _tiles[100];
        MyFrameListener* _frame_listener;
        int _ship_counter;
        bool _human_controlled;
        int _score;
    public:
        Board (MyFrameListener* frame_listener, bool human_controlled);
        ~Board ();
        bool placeShip (int pos, bool horizontal);
        void hit (int pos);
        bool finished ();
        bool isAvailable(int pos, int size, bool horizontal);
        int getNextShip () { return _ship_counter + 1; };
        int getScore();
};

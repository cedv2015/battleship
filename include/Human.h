#include "Player.h"

class Human : public Player {
    private:
        bool _horizontal;
    public:
        Human (Game* game);
        ~Human ();
        virtual void yourTurn ();
        virtual void placeShips ();
        void hit(int pos);
        void changeOrientation ();
        bool getOrientation ();
        int getNextShip () { return _my_board->getNextShip(); };
        bool isAvailable (int pos, int size, bool horizontal) { return _my_board->isAvailable(pos, size, horizontal); };
};

#include <Ogre.h>
#include "MyFrameListener.h"
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "Game.h"
#include "TrackManager.h"

class MyApp {
  
private:
  Ogre::SceneManager* _sceneManager;
  Ogre::Root* _root;
  CEGUI::OgreRenderer* renderer; 
  MyFrameListener* _framelistener;
  RankingManager* _rankingManager;
  TrackManager* _trackManager;
  TrackPtr _mainTrack;
  Game* _game;
  
public:
  MyApp();
  ~MyApp();  
  int start();
  void loadResources();
  void createScene();
  void createGUI();
  void initUI();
  bool initSDL();
};


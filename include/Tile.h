#include "Ship.h"

#define INITIAL 0
#define WATER 1
#define TOUCHED 2
#define DROWN 3

class Tile {
    private:
        int _state;
        Ship* _ship;
    public:
        Tile ();
        ~Tile ();
        bool isEmpty ();
        void setShip (Ship* ship);
        bool hit ();
        void dead ();
        Ship* getShip();
        int getState ();
};
